
def random_memory():
    import random
    import time

    try:
        userinput0 = int(input("Input Memory Count: "))
        if userinput0 > 100000:
            print("Err")
            time.sleep(1)
            random_memory()
    except:
        print("Err")
        time.sleep(1)
        random_memory()

    for x in range(0,userinput0):
        n = random.randint(0x0000000000, 0xffffffffff)
        print("[DEBUG] "+"RAM[{0}] = {1}".format(x+1, hex(n)))
        time.sleep(0.01)
    
random_memory()
